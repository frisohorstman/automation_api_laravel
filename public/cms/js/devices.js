$(document).ready(function() {
    toggleZwaveFieldset();

    $("[name='device[type]']").change(function() {
        toggleZwaveFieldset();
    });
});

function toggleZwaveFieldset() {
    const selected = $("[name='device[type]']").val();
    const zwave_fieldset = $("[name='device[zwave_port]']").closest('fieldset').parent().closest('fieldset')

    if(selected == 'zwave') {
        zwave_fieldset.show();
    } else {
        zwave_fieldset.hide();
    }
}