$(document).ready(function() {
    $('.btn-save').parent().hide();
    $("[name='update']").parent().parent().hide();
    $("[name='fileId']").parent().parent().hide();
    $("[name='cleanName']").parent().parent().hide();

    $('.omdb-search-btn').click(function(e) {
        e.preventDefault();

        var search = $("[name='search']").val();

        $.getJSON("/api/omdb/search", { t: search }, function(result) {
            console.log("JSON Data: ", result);
            placeTable(result);
        });
    })
});

function placeTable(result) {
    html = '';

    Object.entries(result).forEach(entry => {
        const [key, value] = entry;
        html = html + '<tr><td>' + key + '</td><td>' + value + '</td>';
    });

    html = '<table class="table">' + html + '</table>';
    html = '<div class="bg-white rounded shadow-sm mb-3">' + html + '</div>';
    html = '<legend class="text-black">API result</legend>' + html;

    // buttons
    buttons  = '<button class="btn float-start me-2 mb-4 btn-secondary" type="button" onclick="clickSave();">';
    buttons += 'Update 1 file';
    buttons += '</button>';
    buttons += '<button class="btn float-start me-2 mb-4 btn-secondary" type="button" onclick="clickSave(\'all\');">';
    buttons += 'Update all files with clean name "'+ $("[name='cleanName']").val() +'"';
    buttons += '</button>';
    html = html + '<div class="bg-white rounded shadow-sm mb-3">' + buttons + '</div>';
    
    $('#omdbSearchResult').html(html);
}

function clickSave(what = 'one') {
    $("[name='update']").val(what);

    $('.btn-save').parent().show();
    $('.btn-save').click();
}