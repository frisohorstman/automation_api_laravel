<?php

// errors. only uncomment when developing!
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

// import classes
include_once('Fapi.class.php');
include_once('Route.class.php');

// headers so javascript may reach our api
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

// entry point, show api to end user
$fapi = new Fapi();
$fapi->go();