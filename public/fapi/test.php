<?php


class Route
{   
    public $uri;
    public $middle;
    public $appMethod;
    public $routeName;

    function __construct($uri, $appMethod)
    {
        $this->uri = $uri;
        $this->appMethod = $appMethod;
    }

    static function get($uri, $appMethod)
    {
        return new Route($uri, $appMethod);
    }

    static function middleware($middle)
    {
        $route = new Route($uri, $appMethod);
        $route->middle = $middle;
        return $route;
    }

    public function name($routeName)
    {
        $this->routeName = $routeName;
        return $this;
    }
}

// get
$something = Route::get('/iets', 'App\Http\Controllers\OverviewController@index')->name('api.index');

print_r($something);

// middleware
$else = Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
})->name('api.auth');

print_r($else);