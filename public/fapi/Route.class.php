<?php

// this class parses Laravel's api routes and places them in global $routes[]

$routes = [];
$tmp = [];

class Route
{   
    static function middleware($middleware)
    {
        global $tmp;
        $tmp['middleware'] = $middleware;
        return new Route();
    }

    static function get($get)
    {
        global $tmp;
        $tmp['get'] = $get;
        return new Route();
    }

    static function name($name)
    {
        global $tmp;
        global $routes;
        $tmp['name'] = $name;

        // move
        $routes[] = $tmp;
        $tmp = [];

        return new Route();
    }
}

include_once('../../routes/api.php');

// .. now use $routes further down