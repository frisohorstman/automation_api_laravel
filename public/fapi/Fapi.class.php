<?php

class Fapi
{
    private $cache;

    function __construct()
    {
        $this->cache = new \Memcached();
        $this->cache->addServer('localhost', 11211);
    }

    /**
     * entry point via index.php
     */
    function go() {
        // only parse uri if were viewing a fapi subdomain
        if(substr($_SERVER['SERVER_NAME'], 0, 4) == 'fapi') {
            return $this->getUri($_SERVER['REQUEST_URI']);
        }

        // when looking at api.home/fapi folder, meant for dev.
        if(isset($_GET['name'])) {
            return $this->getValue($_GET['name']);
        }
    
        // or.. show overview
        return $this->showKeys();
    }

    /**
     * follow uri
     */
    function getUri($currentUri) {
        if($currentUri == '/api') {
            $currentUri = '/api/';
        }

        $routeName = $this->getUriName($currentUri);

        if($routeName === false) {
            // this should never happen
            echo 'fapi error: uri '.$currentUri.' not found'."\n";
            die();
        }
        
        $cacheKey = 'laravelhomeautomation_cache:'.$routeName;
        $cache = $this->getValue($cacheKey);

        if($cache == false) {
            // there is no cache so goto laravel api domain (remove f)
            $domain = substr($_SERVER['SERVER_NAME'], 1);
            header('Location: http://'.$domain.$_SERVER['REQUEST_URI']);
        }
    }

    /**
     * get Laravel's route name.
     */
    function getUriName($currentUri) {
        global $routes;

        foreach($routes as $route) {
            $routeUri = '/api/'.$route['get'];

            if (preg_match('#^'.preg_replace('/{[^}]*}/','[^/]*',$routeUri).'$#',$currentUri)) {
                if(!isset($route['name'])) {
                    return false;
                }
                
                return $route['name'];
            }
        }

        return false;
    }

    /**
     * overview of memcached keys
     */
    function showKeys() {
        $keys = $this->cache->getAllKeys();

        if(empty($keys)) {
            echo 'There are no memcached keys at this moment. Try invoking an url...';
        } else {
            echo 'keys:<br><br>';
        }

        foreach($keys as $key) {
            echo '<a href="/fapi?name='.$key.'">'.$key.'</a><br>';
        }
    }

    /**
     * request a key's value from memcache and show it via json_encode
     */
    function getValue($key) {
        $value = $this->cache->get($key);

        if($value == false) {
            // cache not found
            return false;
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($value);
        return true;
    }
}