## Installing Awesome Media's Home Automation API

After you've cloned this Laravel git repository:

Make sure you've set up a database for PostgreSQL and installed the php pgsql driver. 
If it's not installed then install it via `apt-get install php7.3-pgsql`.
Create the .env file in the root directory filled with the database credentials.

```
APP_NAME=LaravelHomeAutomation
APP_ENV=local
APP_KEY=base64:gspcaHqnt913U3vRa/Rw24bR3p25MGLvFZLyj+1BdXQ=
APP_DEBUG=true
APP_URL=http://api.server.home

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=pgsql
DB_HOST=
DB_PORT=5432
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120
# CACHE_DRIVER=file
CACHE_DRIVER=memcached

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

SCOUT_DRIVER=null
```

And finally run `composer install && php artisan install`.