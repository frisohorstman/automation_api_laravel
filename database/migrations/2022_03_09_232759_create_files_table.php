<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('filepath');
            $table->string('extension')->nullable();
            $table->bigInteger('size')->nullable();
            $table->string('filename')->nullable();
            $table->string('cleanName')->nullable();
            $table->string('originalFilename')->nullable();
            $table->boolean('isImage')->nullable();
            $table->boolean('isVideo')->nullable();
            $table->boolean('searchedOmdbApi')->nullable();
            $table->string('imdbId')->nullable();
            $table->string('ripSource')->nullable();
            $table->string('codec')->nullable();
            $table->enum('type', ['movie', 'series'])->nullable();
            $table->integer('season')->nullable();
            $table->integer('episode')->nullable();
            $table->string('md5')->nullable();
            $table->timestamp('imported_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
