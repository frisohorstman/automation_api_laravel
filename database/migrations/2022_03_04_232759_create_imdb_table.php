<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImdbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imdb', function (Blueprint $table) {
            $table->id();
            $table->string('imdbId')->nullable();
            $table->float('imdbRating')->nullable();
            $table->string('imdbVotes')->nullable();
            $table->string('title')->nullable();
            $table->year('year')->nullable();
            $table->string('rated')->nullable();
            $table->date('released')->nullable();
            $table->string('runtime')->nullable();
            $table->json('genre')->nullable();
            $table->json('director')->nullable();
            $table->json('writer')->nullable();
            $table->json('actors')->nullable();
            $table->string('plot')->nullable();
            $table->json('language')->nullable();
            $table->json('country')->nullable();
            $table->string('awards')->nullable();
            $table->string('poster')->nullable();
            $table->json('ratings')->nullable();
            $table->integer('metascore')->nullable();
            $table->string('type')->nullable();
            $table->date('dvd')->nullable();
            $table->string('boxOffice')->nullable();
            $table->string('production')->nullable();
            $table->string('website')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imdb');
    }
}
