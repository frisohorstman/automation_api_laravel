<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoiceFunctionsTable extends Migration
{
    public function boot()
    {
        Schema::defaultStringLength(150);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voice_functions', function (Blueprint $table) {
            $table->id();
            $table->string('module', 50)->nullable();
            $table->string('class', 50)->nullable();
            $table->string('function', 50);
            $table->string('arg1', 150)->nullable();
            $table->string('arg2', 150)->nullable();
            $table->longtext('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voice_functions');
    }
}
