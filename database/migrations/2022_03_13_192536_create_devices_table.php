<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('mac');
            $table->string('ip')->nullable();
            $table->boolean('online')->default(false);
            $table->string('cpu')->nullable();
            $table->json('memory')->nullable();
            $table->enum('type', ['zwave', 'jarvis', 'media_server'])->nullable();

            $table->string('zwave_port')->nullable();
            $table->string('zwave_username')->nullable();
            $table->string('zwave_password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
