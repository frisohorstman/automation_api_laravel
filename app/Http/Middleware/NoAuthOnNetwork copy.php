<?php

/**
 * http requests via our network are ok.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
// use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
// use Laravel\Sanctum\Http\Middleware\CheckForAnyAbility;
use App\Models\User;
// use Laravel\Sanctum;

class NoAuthOnNetwork
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return ResponseFactory|RedirectResponse|Response|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ip = ip2long($request->ip());
        $low_ip = ip2long('10.0.0.1');
        $high_ip = ip2long('10.0.0.255');

        // if ($ip <= $high_ip && $low_ip <= $ip) {
        //     // you're on the home network, continue app's next request
        //     return $next($request);
        // }

        $token = $request->bearerToken();
        // $user = User::where('api_token', $token)->first();

        // $guard = Authenticate::class;
        // dd($guard);

        return app(Authenticate::class)->handle($request, function ($request) use ($next) {
            echo "weee";die();
            // return route('login');
            return $next($request);
        });


    }


    /**
     * Redirect on the application login form.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|ResponseFactory|RedirectResponse|Response
     */
    protected function redirectToLogin(Request $request)
    {
        if ($request->ajax() || $request->wantsJson()) {
            return response('Unauthorized.', 401);
        }

        return 'admin';
    }
}
