<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\VoiceFunctions;
use App\Models\VoiceKeywords;

class VoiceFunctionsController extends Controller
{
    public function index()
    {
        return Cache::rememberForever(Route::currentRouteName(), function() {
            $all = [];
            foreach(VoiceFunctions::all() as $func) {
                $func->keywords = VoiceKeywords::where('function_id', $func->id)->get()->pluck('keyword');
                $all[] = $func;
            }

            return $all;
        });
    }

    
}