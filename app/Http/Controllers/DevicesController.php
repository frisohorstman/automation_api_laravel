<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Resources\DevicesResource;
use App\Models\Devices;
 
class DevicesController extends Controller
{
    // hide these fields on our api.
    // @note: dont forget to include the function when you expose a device...
    // @todo: 
    public $hideFields = [
        'zwave_port',
        'zwave_username',
        'zwave_password'
    ];

    public function index()
    {
        return Devices::all()->makeHidden($this->hideFields);
    }

    /**
     * gives the device on ID 
     */
    public function show(Devices $device, Request $request)
    {
        if($request->get('control')) {
            return $this->control($device, $request);
        }

        return $device->makeHidden($this->hideFields);
    }

    /**
     * return device
     * 
     * check _GET[type] and give device
     */
    public function get(Request $request)
    {
        if(!$request->get('type')) {
            return ['success' => false];
        }

        return Devices::where('type', $request->get('type'))->first();
    }
    
    /**
     * act upon _GET[control].
     */
    public function control($device, $request)
    {
        if($request->get('control') != 'reboot') {
            return [
                'success' => false,
                'error' => 'Unrecognized control action'
            ];
        }

        // exec shell command
        $cmd = 'sudo reboot';
        if($device->os == 'windows') {
            $cmd = 'shutdown /r /t 3 /c "Home automation wants to reboot."';
        }

        $output = null;
        exec($cmd, $output, $retval);

        if($retval !== 0) {
            return [
                'success' => false,
                'cmd' => $cmd,
                'error' => 'Return val was not 0'
            ];
        }

        return [
            'success' => true,
            'output' => $output
        ];
    }
}