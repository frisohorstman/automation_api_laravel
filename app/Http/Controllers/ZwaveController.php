<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Devices;
 
class ZwaveController extends Controller
{
    public function index()
    {
        $dev = Devices::where('type', 'zwave')->first()->toArray();

        if(empty($dev)) {
            return [
                'success' => false,
                'error' => 'Z-wave not found.'
            ];
        }

        // if(!isset($_GET['url'])) {
        //     return [
        //         'success' => false,
        //         'error' => 'Z-wave API url not found.'
        //     ];
        // }

        // do call on network device
        // $url = 'http://'.$dev['ip'].'/'.$_GET['url'];
        $url = 'http://'.$dev['ip'].'/ZAutomation/api/v1/devices';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, $dev['zwave_port']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_USERPWD, $dev['zwave_username'].':'.$dev['zwave_password']);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}