<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function index()
    {   
        return Cache::rememberForever(Route::currentRouteName(), function() {
            $routes = [];
            foreach (Route::getRoutes()->getIterator() as $route){
                if (substr($route->uri, 0, 3) == 'api') {
                    if($route->uri == 'api') { // uri we're on
                        continue;
                    }
                    
                    $routes[$route->uri] = [
                        'url' => \Request::root().'/'.$route->uri
                    ];
                }
            }

            return $routes;
        });
    }
}