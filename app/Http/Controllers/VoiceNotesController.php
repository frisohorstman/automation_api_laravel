<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\VoiceNotes;

class VoiceNotesController extends Controller
{
    public function index()
    {
        return Cache::rememberForever(Route::currentRouteName(), function() {
            return VoiceNotes::all()->toArray();
        });
    }
}