<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
class InfraredController extends Controller
{
    /**
     * overview of all remotes and it's keys
     */
    public function index()
    {
        $lirc_conf_folder = '/etc/lirc/lircd.conf.d/';

        if(!file_exists($lirc_conf_folder)) {
            return [
                'success' => false,
                'error' => 'Lirc config directory does not exist. (are you using the wrong server?)'
            ];
        }

        $files = scandir($lirc_conf_folder);
        $remotes = array();

        foreach($files as $file) {
            if(substr($file, -10) != 'lircd.conf') {
                continue;
            }

            $remote = $this->parseConfig($lirc_conf_folder.$file);
            list($name, $keys) = $remote;

            $remotes[] = array(
                'remote_name' => $name,
                'keys' => $keys,
            );
        }

        return $remotes;
    }

    /**
     * emit an infrared signal via Lirc
     */
    function send(Request $request) {
        if(!$request->get('remote') || !$request->get('key')) {
            return [
                'success' => false,
                'error' => 'Infrared remote or key was not given.'
            ];
        }

        // execute command
        $cmd = 'irsend -a localhost:2220 SEND_ONCE '.$request->get('remote').' '.$request->get('key');

        $output = null;
        exec($cmd, $output, $retval);

        if($retval !== 0) {
            return [
                'success' => false,
                'cmd' => $cmd,
                'error' => 'Something went wrong emitting the infrared signal.'
            ];
        }

        return [
            'success' => true,
            'cmd' => $cmd,
            'info' => 'Infrared signal has been emitted.',
        ];
    }

    /**
     * insert string; $config_filepath
     * return array;
     * 
     * parse a lirc config file to get all infrared keys
     * (helper function)
     */
    function parseConfig($config_filepath = false) {
        $conf = file_get_contents($config_filepath);

        // iterate over each line
        $separator = "\r\n";
        $line = strtok($conf, $separator);
        $reached_codes = false;
        $remote_name = false;
        $remote_keys = array();

        while ($line !== false) {
            $line = strtok($separator);

            // get the remote name from the config file
            if(substr(trim($line), 0, 4) == 'name') {
                $remote_name = substr(trim($line), 6);
            }

            if(trim($line) == 'begin codes') {
                $reached_codes = true;
                continue;
            }

            if(trim($line) == 'end codes') {
                $reached_codes = false;
                continue;
            }

            if($reached_codes == false) {
                continue;
            }

            // these are the remote keys, put them in an array
            list($key, $code) = preg_split('/\s+/', trim($line));
            $remote_keys[$key] = $code;
        }

        return array($remote_name, $remote_keys);
    }
}