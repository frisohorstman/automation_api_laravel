<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Models\Files;
 
class FilesController extends Controller
{
    public function index()
    {
        return Cache::rememberForever(Route::currentRouteName(), function() {
            return Files::all()->toArray();
        });
    }
 
    public function show(Files $file)
    {
        return $file;
    }

    public function download(Files $file)
    {
        $size = filesize($file->filepath);

        // multipart-download and download resuming support
        if (isset($_SERVER['HTTP_RANGE'])) {
            list($a, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
            list($range) = explode(',', $range, 2);
            list($range, $range_end) = explode('-', $range);
            $range = intval($range);
            if (!$range_end) {
                $range_end = $size - 1;
            }
            else {
                $range_end = intval($range_end);
            }

            $new_length = $range_end - $range + 1;
            \header('HTTP/1.1 206 Partial Content');
            \header('Content-Length: '.$new_length);
            \header("Content-Range: bytes {$range}-{$range_end}/{$size}");
        } else {
            $new_length = $size;
            \header('Content-Length: '.$size);
        }

        /* output the file itself */
        $chunksize = 1024*1024;
        $bytes_sent = 0;

        \header('X-No-Pirate: Please do not steal our content.');

        while (ob_get_level()) {
            ob_end_clean();
        }
        
        $fz = fopen($file->filepath, 'rb');
        if ($fz) {
            if (isset($_SERVER['HTTP_RANGE'])) {
                fseek($fz, $range);
            }

            while (!feof($fz) && (!connection_aborted()) && ($bytes_sent < $new_length) ) {
                $buffer = fread($fz, $chunksize);
                echo $buffer;
                flush();
                $bytes_sent += strlen($buffer);
            }
            fclose($fz);
        }
        else {
            throw new \Exception('Error - can not open file.');
        }
    }

    /**
     * mark a file as watched
     */
    public function watched(Request $request) {
        $file = Files::where('filepath', $request->get('filepath'))->first();

        if(empty($file)) {
            return [
                'success' => false,
                // 'error' => 'Could not mark file as watched, because it was not found.'
            ];
        }

        $file->watched = true;
        if($file->save()) {
            return [
                'success' => true,
            ];
        } else {
            return [
                'success' => false,
                'error' => 'Could not update database.'
            ];
        }
    }
}