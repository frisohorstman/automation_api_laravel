<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\OmdbApi;
 
class OmdbApiController extends Controller
{
    public function search(Request $request)
    {
        if(empty($request->get('t'))) {
            return ['Response' => 'False', 'Searched' => 'False'];
        }

        $api = new OmdbApi();
        $year = false;
        
        if(!empty($request->get('y'))) {
            $year = $request->get('y');
        }

        $result = $api->search($request->get('t'), $year);

        if($result == false) {
            return ['Response' => 'False', 'Searched' => 'True'];
        }

        return $result;
    }
}