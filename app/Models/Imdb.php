<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as EloquentModel;

use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

abstract class Model extends EloquentModel
{
    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        if ($this->getCastType($key) == 'array' && is_null($value)) {
            return [];
        }

        return parent::castAttribute($key, $value);
    }
}

class Imdb extends Model
{
    use Filterable, AsSource, HasFactory;

    // database table to use
    protected $table = 'imdb';

    /**
     * Name of columns to which http sorting can be applied
     *
     * @var array
     */
    protected $allowedSorts = [
        'title',
        'created_at'
    ];

    protected $allowedFilters = [
        'title',
    ];
    
    // transform these json database fields to array's
    protected $casts = [
        'genre' => 'array',
        'language' => 'array',
        'country' => 'array',
        'ratings' => 'array',
        'director' => 'array',
        'writer' => 'array',
        'actors' => 'array'
    ];

    /**
     * input string; $cleanMovieTitle
     * return array; $result
     * 
     * Find movie a case insensitive movie title in imdb table 
     */
    static function searchTitle($title = false)
    {
        return self::where('title', 'ILIKE', $title)->first();
    }


    /**
     * input array; $omdbApiResult
     * return boolean
     * 
     * Insert an OmdbApi array into our Imdb database table
     */
    static function saveOmdbMovie($omdb)
    {   
        // check if we dont have it already
        if(!empty(Imdb::firstWhere('imdbId', $omdb['imdbID']))) {
            return false;
        }

        // add
        $imdb = new Imdb();

        $imdb->imdbId = $omdb['imdbID'];

        if($omdb['imdbRating'] != 'N/A') {
            $imdb->imdbRating = $omdb['imdbRating'];
        }

        if($omdb['imdbVotes'] != 'N/A') {
            $imdb->imdbVotes = $omdb['imdbVotes'];
        }
        
        $imdb->title = $omdb['Title'];
        $imdb->year = filter_var($omdb['Year'], FILTER_SANITIZE_NUMBER_INT);

        if($omdb['Rated'] != 'N/A') {
            $imdb->rated = $omdb['Rated'];
        }
        
        $imdb->released = Imdb::parseOmdbDate($omdb['Released']);

        if($omdb['Runtime'] != 'N/A') {
            $imdb->runtime = $omdb['Runtime'];
        }

        if($omdb['Genre'] != 'N/A') {
            $imdb->genre = explode(', ', $omdb['Genre']);
        }

        if($omdb['Director'] != 'N/A') {
            $imdb->director = explode(', ', $omdb['Director']);
        }

        if($omdb['Writer'] != 'N/A') {
            $imdb->writer = explode(', ', $omdb['Writer']);
        }

        if($omdb['Actors'] != 'N/A') {
            $imdb->actors = explode(', ', $omdb['Actors']);
        }

        if($omdb['Plot'] != 'N/A') {
            $imdb->plot = $omdb['Plot'];
        }

        if($omdb['Language'] != 'N/A') {
            $imdb->language = explode(', ', $omdb['Language']);
        }

        if($omdb['Country'] != 'N/A') {
            $imdb->country = explode(', ', $omdb['Country']);
        }

        if($omdb['Awards'] != 'N/A') {
            $imdb->awards = $omdb['Awards'];
        }

        if($omdb['Poster'] != 'N/A') {
            $imdb->poster = $omdb['Poster'];
        }

        if($omdb['Ratings'] != 'N/A') {  
            $imdb->ratings = $omdb['Ratings'];
        }

        if($omdb['Type'] != 'N/A') {
            $imdb->type = $omdb['Type'];
        }

        if($omdb['Metascore'] != 'N/A') {
            $imdb->metascore = $omdb['Metascore'];
        }

        if(isset($omdb['DVD']) && $omdb['DVD'] != 'N/A') {
            $imdb->dvd = Imdb::parseOmdbDate($omdb['DVD']);
        }

        if(isset($omdb['BoxOffice']) && $omdb['BoxOffice'] != 'N/A') {
            $imdb->boxOffice = $omdb['BoxOffice'];
        }

        if(isset($omdb['Production']) && $omdb['Production'] != 'N/A') {
            $imdb->production = $omdb['Production'];
        }

        if(isset($omdb['Website']) && $omdb['Website'] != 'N/A') {
            $imdb->website = $omdb['Website'];
        }
        
        $imdb->created_at = \Carbon\Carbon::now();
        $imdb->updated_at = NULL;
        
        return $imdb->save();
    }

    static function parseOmdbDate($str = false)
    {
        if($str == 'N/A') {
            return NULL;
        }

        $date = strtotime($str);

        if($date == false) {
            return NULL;
        }

        $dt = new \DateTime($str);
        return $dt;
    }
}
