<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Orchid\Screen\AsSource;

class Devices extends Model
{
    use HasFactory, AsSource;

    // transform json into array
    protected $casts = [
        'memory' => 'array'
    ];
}
