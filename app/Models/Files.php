<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

use Owenoj\LaravelGetId3\GetId3;

class Files extends Model
{
    use Filterable, HasFactory; // AsSource, 

    /**
     * Columns get converted to Carbon so you can use ->diffForHumans() on these fields
     */
    protected $dates = [
        'imported_at',
        'created_at',
        'updated_at'
    ];

    /**
     * Name of columns to which http sorting can be applied
     *
     * @var array
     */
    protected $allowedSorts = [
        'filename',
        'size',
        'created_at'
    ];

    protected $allowedFilters = [
        'filename',
        'imdbId'
    ];


    protected $id3;

    /**
     * path
     */
    public function getPathAttribute()
    {
        return pathinfo($this->attributes['filepath'])['dirname'];
    }

    /**
     * folderName of path.. or the folder where this file is located in. but only its name!  
     */
    public function getFolderNameAttribute()
    {
        $dirpath = pathinfo($this->attributes['filepath'])['dirname'];
        return basename($dirpath);
    }

    /**
     * id3
     */
    public function getId3Attribute()
    {
        if(!empty($this->id3)) {
            return $this->id3;
        }
        
        $id3 = new GetId3($this->attributes['filepath']);
        $this->id3 = $id3->extractInfo();

        return $this->id3;
    }

    /**
     * gives back an api url to download the file
     */
    public function getApiDownloadUrlAttribute()
    {
        return route('api.files.download', $this->attributes['id']);
    }

    /**
     * mime content type
     */
    public function getContentTypeAttribute()
    {
        return mime_content_type($this->attributes['filepath']);
    }

    /**
     * add 'humanSize' field where a human readable file size is presented
     * in the form of xx GB/xx MB/xx KB/xx bytes
     */
    public function getSizeHumanAttribute()
    {
        $bytes = $this->attributes['size'];

        if ($bytes >= 1073741824)
        {
            return number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            return number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            return number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            return $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            return $bytes . ' byte';
        }
        else
        {
            return '0 bytes';
        }
    }
}
