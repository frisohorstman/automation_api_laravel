<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class VoiceFunctions extends Model
{
    // use HasFactory, AsSource;
    use Filterable, HasFactory, AsSource; 

    protected $table = 'voice_functions';

    /**
     * Get the keywords for this function.
     */
    public function keywords()
    {
        return $this->hasMany(VoiceKeywords::class, 'function_id');
    }
}
