<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CacheShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:show {cacheKey?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all memcached keys that are stored in memory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $c = new \Memcached();
        $c->addServer("localhost", 11211);

        if(empty($this->argument('cacheKey'))) {
            // overview of keys
            $keys = $c->getAllKeys();
            if(empty($keys)) {
                $this->info('There are no memcached keys at this moment. Try invoking an url...');
            }

            foreach($keys as $key) {
                $this->info($key);
            }
        } else {
            // show value when key is provided
            $value = $c->get($this->argument('cacheKey'));
            $this->info(print_r($value, true));
        }

        return 0;
    }
}
