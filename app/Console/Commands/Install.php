<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install all packages and migrate database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run all install's..
     */
    public function handle()
    {
        // $this->exec('npm cache clear --force');
        // $this->exec('rm package-lock.json yarn.lock');
        // $this->exec('rm -rf node_modules');
        // $this->exec('rm -rf vendor/*');

        // $this->exec('composer install'); --> you might need to run this one by hand..

        $this->exec('composer require laravel/jetstream');
        $this->exec('php artisan jetstream:install livewire');
        $this->exec('php artisan vendor:publish --tag=jetstream-views');
        $this->exec('npm install');
        $this->exec('npm run dev');
        $this->exec('yes | php artisan orchid:install');
        $this->exec('php artisan migrate:fresh');

        echo "\nNow you should create an Admin user:\n";
        echo "`php artisan orchid:admin admin admin@admin.com password`\n\n";
        echo "And run `php artisan serve` to start the artisan webserver.\n";
    }

    /**
     * Execute a single console command.
     */
    public function exec($cmd)
    {
        passthru($cmd);
    }
}
