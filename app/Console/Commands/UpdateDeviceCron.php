<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Devices;

class UpdateDeviceCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:update-device {--register} {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update this device\'s information in the database. Memory useage fluctuates.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {        
        $device = Devices::where('mac', $this->getMac())->first();

        if(empty($device) && $this->option('register') !== true) {
            // update database record
            $this->error('ERROR: The device was not found.');
            echo "Use `php artisan ".$this->argument('command')." --register` to register a new device. \n";
            return false;
        }

        if(empty($device) && $this->option('register') === true) {
            // add database record
            $device = new Devices;
            
            $device->name = $this->getMac();
            if(!empty($this->argument('name'))) {
                $device->name = $this->argument('name');
            }
        }
        
        $device->online = true;
        $device->cpu = $this->getCpu();
        $device->mac = $this->getMac();
        $device->memory = $this->getMemory();
        $device->ip = $this->getIp();
        $device->save();

        $this->info("This device has been saved.");

        return true;
    }


    public function getIp()
    {
        return trim(shell_exec("ifconfig | grep 'inet' | grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $2}'"));
    }

    public function getMemory()
    {
        $mem = file_get_contents('/proc/meminfo');

        $memTotal = $this->findKey('MemTotal', $mem);
        $memTotal = (int)filter_var($memTotal, FILTER_SANITIZE_NUMBER_INT);
        $memAvailable = $this->findKey('MemAvailable', $mem);
        $memAvailable = (int)filter_var($memAvailable, FILTER_SANITIZE_NUMBER_INT);
        $memUsed = $memTotal - $memAvailable;

        return [
            'total' => $memTotal,
            'used' => $memUsed,
            'available' => $memAvailable,
        ];
    }

    public function getCpu()
    {
        $cpuinfo = file_get_contents('/proc/cpuinfo');
        return $this->findKey('model name', $cpuinfo);
    }

    public function getMac()
    {
        // /sys/class/net/eno1/address
        $root = '/sys/class/net';
        $scanned = scandir($root);

        foreach($scanned as $dir) {
            if($dir == '.' || $dir == '..' || $dir == 'lo') {
                continue;
            }

            $mac = file_get_contents($root.'/'.$dir.'/address');
            return strtoupper(trim($mac));
        }
        
        return false;
    }

    /**
     * input string; $findKey
     * return string; $foundValue
     * 
     * loop trough text and split each line on :
     */
    public function findKey($findKey, $text) {
        $separator = "\r\n";
        $line = strtok($text, $separator);
        
        while ($line !== false) {
            list($key, $val) = explode(':', $line);

            if(trim($key) == $findKey) {
                return trim($val);
            }

            $line = strtok($separator);
        }

        return false;
    }
}
