<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\VoiceFunctions;

class TryoutCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tryout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tiny tests or temp stuff are done inhere';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach(VoiceFunctions::all() as $f) {
            if(!isset($f->keywords[0]->created_at) || empty($f->keywords[0]->created_at)) {
                continue;
            }

            $f->created_at = $f->keywords[0]->created_at;
            $f->save();
        }
    }
}
