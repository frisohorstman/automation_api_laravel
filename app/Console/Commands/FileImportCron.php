<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Owenoj\LaravelGetId3\GetId3;

// models
use App\Models\Imdb;
use App\Models\Files;

// external classes
use App\Classes\OmdbApi;
use App\Classes\TorrentName;

class FileImportCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:file-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all movies and jpg files from Download folder';

    // the directory that will be scanned
    protected $rootDir = NULL;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->rootDir = '/mnt/internal/downloads_disk/Downloads';
        $this->scanDir($this->rootDir);
    }

    /**
     * output message to cli and to log
     */
    public function inform($msg, $func = 'info')
    {
        \Log::$func($msg);
        $this->$func($msg);
    }

    public function scanDir($root = false) {
        $dir = scandir($root);

        // scan it recursively to get the files inside directories
        foreach($dir as $filename) {
            // skip the unix dots
            if($filename == '.' || $filename == '..') {
                continue;
            }

            $filepath = $root.'/'.$filename;
            
            // import filepath or recursively scan further..
            if(is_file($filepath)) {
                $this->importFilepath($filepath, $root);
            } else {
                // echo "hier zit een recursive scandir!\n";
                $this->scanDir($filepath);
            }
        }
    }

    public function importFilepath($filepath, $root) {
        // only import images and videos
        $mimeType = explode('/', mime_content_type($filepath))[0];

        if($mimeType != 'image' && $mimeType != 'video') {
            return false;
        }

        // skip if sample is in filename
        if(strpos(strtolower($filepath), 'sample') !== false) {
            return false;
        }

        // check if filepath is already in database
        if(!empty(Files::firstWhere('filepath', $filepath))) {
            $this->inform('CRON: Filepath "'.$filepath.'" is already in db.', 'error');
            return false;
        }
        
        $clean = TorrentName::parse(basename($filepath));

        if($this->rootDir != $root) {
            // use the first dirname upwards from $this->rootDir.
            // sometimes the filename is jibberish but could contain season info.
            // the dirname is mostly okay, so we use this instead...
            $sub = str_replace($this->rootDir.'/', '', $filepath);
            list($firstName) = explode('/', $sub);
            $first = TorrentName::parse($firstName);

            $clean['title'] = $first['title'];
        }

        $imdbId = NULL;
        $searched = false;

        // clean[type] is only set if its a movie / series
        if($mimeType == 'video' && isset($clean['type'])) {
            $result = $this->importOmdb($clean);
            $imdbId = $result['imdbId'];
            $searched = $result['searched'];
        }

        // add scanned file to `files` table
        $this->inform('CRON: File import to database: '.basename($filepath));

        $file = new Files();
        $file->filepath = $filepath;
        $file->size = filesize($filepath);
        $file->extension = pathinfo($filepath, PATHINFO_EXTENSION);
        $file->filename = basename($filepath);
        $file->originalFilename = basename($filepath);
        $file->cleanName = $clean['title'];
        $file->imdbId = $imdbId;
        $file->searchedOmdbApi = $searched;

        if(isset($clean['type'])) {
            $file->type = $clean['type'];
        }

        if(isset($clean['season'])) {
            $file->season = $clean['season'];
        }

        if(isset($clean['episode'])) {
            $file->episode = $clean['episode'];
        }

        if(isset($clean['source'])) {
            $file->ripSource = $clean['source'];
        }

        if(isset($clean['codec'])) {
            $file->codec = $clean['codec'];
        }

        // check if its a video
        $file->isVideo = false;
        if($mimeType == 'video') {
            $file->isVideo = true;
        }

        // check if its an image
        $file->isImage = false;
        if($mimeType == 'image'){
            $file->isImage = true;
        }

        $file->md5 = @md5_file($filepath);
        $file->imported_at = \Carbon\Carbon::now();
        $file->created_at = filemtime($filepath);
        $file->updated_at = NULL;

        return $file->save();
    }

    /**
     * insert array; TorrentName::parse()
     * return array/false;
     */
    public function importOmdb($clean) {
        // see if we already have a file with our clean title
        $found = Files::firstWhere('cleanName', $clean['title']);

        if(!empty($found)) {
            $this->inform('CRON: Omdb API search not needed. "'.$clean['title'].'" already exists.', 'error');

            return [
                'imdbId' => $found->imdbId,
                'searched' => false
            ];
        }

        // search omdb api
        $omdb = new OmdbApi();

        $year = false;
        if(isset($clean['year'])) {
            $year = $clean['year'];
        }

        $result = $omdb->search($clean['title'], $year);

        if($result == false) {
            $this->inform('CRON: OmdbApi found nothing for: '.$clean['title'], 'error');

            return [
                'imdbId' => NULL,
                'searched' => true
            ];
        }

        // inform and save omdb movie
        $this->inform('CRON: Imdb database import '.$result['imdbID'].': "'.$result['Title'].'"');
        Imdb::saveOmdbMovie($result);

        return [
            'imdbId' => $result['imdbID'],
            'searched' => true,
        ];
    }
}
