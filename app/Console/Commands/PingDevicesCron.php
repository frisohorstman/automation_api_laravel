<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Devices;

class PingDevicesCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:ping-devices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ping all devices from database\'s devices table to see if they\'re online.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {        
        $devices = Devices::all();

        foreach($devices as $device) {
            $device->online = $this->isOnline($device->ip);
            $device->save();
        }

        return true;
    }

    public function isOnline($ip)
    {
        $timeout = 1;
        $fp = @fSockOpen($ip, 22, $errno, $errstr, $timeout); 
        return $fp!=false;
    }
}
