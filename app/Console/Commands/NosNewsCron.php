<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Nos;

class NosNewsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:news-import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Place newest NOS RSS feed into database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->importRss();
        $this->deleteOld();

        return 0;
    }

    /**
     * return array;
     *
     * gets rss feed from nos website, this fetch sometimes fails,
     * so this fetch should goto database if it succeeds
     */
    function getRss() {
        $url = 'http://feeds.nos.nl/nosnieuwsalgemeen';
        $feed = file_get_contents($url);

        return $feed;
    }

    /**
     * import rss feed to our database
     */
    function importRss() {
        $rss = $this->getRss();
        $feed = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);

        foreach($feed->channel->item as $item) {
            // check if link doesnt already exist
            $exists = Nos::where('link', $item->link)->first();

            if(!empty($exists)) {
                continue;
            }

            // add
            $db = new Nos();
            $db->title = $item->title;
            $db->link = $item->link;
            $db->description = $item->description;
            $db->image = $item->enclosure->attributes()->url;
            $db->created_at = new \DateTime($item->pubDate);
            $db->save();

            $this->info('CRON: NOS News Added "'.$item->title.'" to database table');
        }
    }

    /**
     * remove old items from database so it doesnt get too full
     */
    function deleteOld() {
        $max = 20;
        $count = Nos::count();

        if($count < 20) {
            return false;
        }

        // get records to remove
        $records = Nos::orderBy('created_at', 'ASC')->limit($count - $max)->get();

        foreach($records as $record) {
            $this->info('CRON: NOS News Deleting "'.$record->title.'" from database table');
            $record->delete();
        }
    }
}
