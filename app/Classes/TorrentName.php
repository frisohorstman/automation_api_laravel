<?php

namespace App\Classes;

class TorrentName {
    static function patterns() {
        return [
            'season' => '/\b(?i)S\d+\b/m', // S01, S02, etc
            'se' => '/\b(?i)S\d+E(\d+)(?i-)\b/m', // S01E01, etc
            'dateAmerica' => '/(\d{4}.\d{2}.\d{2})/', // 2002-01-01
            'dateEurope' => '/(\d{2}.\d{2}.\d{4})/', // 01-01-2002
            'year' => '/\b(19|20)\d{2}\b/m', // year 19** and 20**
            'resolution' => '([0-9]{3,4}p)', // 720p, etc
            'source' => '((?:PPV\.)?[HP]DTV|(?:HD)?CAM|B[DR]Rip|(?:HD-?)?TS|(?:PPV )?WEB-?DL(?: DVDRip)?|HDRip|DVDRip|DVDRIP|CamRip|W[EB]BRip|BluRay|DvDScr|hdtv|telesync)',
            'codec' => '(xvid|[hx]\.?26[45])', // h264, etc
            'audio' => '(MP3|DD5\.?1|Dual[\- ]Audio|LiNE|DTS|AAC[.-]LC|AAC(?:\.?2\.0)?|AC3(?:\.5\.1)?)',
        ];
    }

    static function parse($filename) {
        $info = array();

        foreach(TorrentName::patterns() as $name => $regex) {
            preg_match_all($regex, $filename, $matches);

            if(isset($matches[0][0])) {
                $info[$name] = $matches[0][0];

                // if a movie is named just a year, like the movie 1917, then 
                // grab the second year from the torrent aka $lastYear
                if($name == 'year' && count($matches[0]) > 1) {
                    $index = array_key_last($matches[0]);
                    $lastYear = $matches[0][$index];
                    $info['year'] = $lastYear;
                }
            }
        }

        ////
        // if dateAmerican or dateEurope is set then the match on info[year] is wrong, see next comment
        if(isset($info['dateAmerica']) || isset($info['dateEurope'])) {
            unset($info['dateAmerica']);
            unset($info['dateEurope']);
            unset($info['year']);
        }

        ////
        // now with the info weve got get everything to the left of the year in filename
        // this will be our title. this works on movie torrents
        if(isset($info['season'])) { // S01, S02, etc
            // series
            $parts = explode('.'.$info['season'].'.', $filename);
            $info['title'] = trim(str_replace('.', ' ', $parts[0])); // replace dots with spaces
            if(isset($info['year'])) {
                $info['title'] = trim(str_replace($info['year'], '', $info['title'])); // remove year
            }
            
            // type
            $info['type'] = 'series';
        } else if(isset($info['se'])) { // S01E01, S03E06, etc
            // series
            $parts = explode('.'.$info['se'].'.', $filename);
            $info['title'] = trim(str_replace('.', ' ', $parts[0])); // replace dots with spaces
            if(isset($info['year'])) {
                $info['title'] = trim(str_replace($info['year'], '', $info['title'])); // remove year
            }
            
            // split se up to season episode
            $parts = explode('E', strtoupper($info['se']));
            $info['season'] = filter_var($parts[0], FILTER_SANITIZE_NUMBER_INT);

            $info['episode'] = $parts[1];
            unset($info['se']);

            // type
            $info['type'] = 'series';
        } else if(isset($info['year'])) {
            // movie
            $parts = explode('.'.$info['year'].'.', $filename);
            
            if(count($parts) == 2) {
                // we got 2 parts, left side is the title with dots between it
                $info['title'] = trim(str_replace('.', ' ', $parts[0]));
            } else {
                // maybe the year is between parentheses, try to split it on this
                $parts = explode('('.$info['year'].')', $filename);
                $info['title'] = trim($parts[0]);
            }

            // type
            $info['type'] = 'movie';
        } else {
            // could not split on year or se. retain original filename
            $info['title'] = $filename;
        }

        return $info;
    }
}
