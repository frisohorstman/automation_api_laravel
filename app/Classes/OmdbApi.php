<?php

namespace App\Classes;

class OmdbApi {
    /**
     * input string, int; $searchTitle, $year
     * return array;
     * 
     * search movie via the omdb api.
     * the title should already be normalized upon input.
     */
     public function search($searchTitle, $year = false) {
        $client = new \GuzzleHttp\Client();

        $query = [
            'apikey' => '7d836747',
            't' => $searchTitle,
        ];

        if($year !== false) {
            $query['y'] = $year;
        }

        $url = 'http://www.omdbapi.com/?'.http_build_query($query);
        // echo 'INFO: Querying OMDB API '.$url."\n"; // dont echo this on production

        // do the actual request
        $request = $client->get($url);
        $json = $request->getBody()->getContents();
        $result = json_decode($json, true);

        if($result['Response'] == "False") {
            return false;
        }

        return $result;
    }
}
