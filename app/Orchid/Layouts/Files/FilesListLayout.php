<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Files;

use Illuminate\Http\Request;

use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use Orchid\Support\Color;

use App\Models\Files;
use App\Models\Imdb;

class FilesListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'files';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        $tds = [
            TD::make('path', __('Path'))
                ->sort()
                ->defaultHidden()
                ->render(function (Files $file) {
                    return pathinfo($file->filepath)['dirname'].'/';
                }),

            TD::make('filename', __('Filename'))
                ->sort()
                ->cantHide()
                ->width(700)
                ->filter(Input::make())
                ->render(function (Files $file) {
                    return Link::make($file->filename)
                        ->route('platform.systems.files.edit', $file->id);
                }),

            TD::make('seasonEpisode', '')
                ->sort()
                ->render(function (Files $file) {
                    $txt = '';
                    if(!empty($file->season)) {
                        $txt .= 'S'.sprintf("%02d", $file->season);
                    }

                    if(!empty($file->episode)) {
                        $txt .= ' E'.sprintf("%02d", $file->episode);
                    }
                    
                    return $txt;
                }),

            TD::make('watched', '')
                ->sort()
                ->render(function (Files $file) {
                    if($file->watched) {
                        return Link::make('')->icon('eye')->type(Color::DEFAULT());
                    }
                }),

            TD::make('type', __('Type'))
                ->cantHide()
                ->render(function (Files $file) {
                    if($file->isVideo) {
                        return Link::make('')->icon('video')->type(Color::DEFAULT());
                    }

                    if($file->isImage) {
                        return Link::make('')->icon('picture')->type(Color::DEFAULT());
                    }
                }),

            TD::make('size', __('Size'))
                ->sort()
                ->render(function (Files $file) {
                    return $file->sizeHuman;
                }),

            TD::make('created_at', __('Created'))
                ->sort()
                ->render(function (Files $file) {
                    return $file->created_at->diffForHumans();
                }),
        ];

        $tds[] = TD::make('')
            ->align(TD::ALIGN_CENTER)
            ->width('30px')
            ->render(function (Files $file) {
                // dropdown buttons
                $btns = [
                    Link::make(__('File Page'))
                        ->route('platform.systems.files.edit', $file->id)
                        ->icon('pencil')
                ];

                // imdb buttons
                if(!empty($file->imdbId)) {
                    // goto movie admin page
                    if(\Request::route()->getName() != 'platform.systems.movies.edit') {
                        $imdb = Imdb::where('imdbId', $file->imdbId)->first();

                        if(!empty($imdb)) {
                            $btns[] = Link::make('Movie Page')
                                    ->route('platform.systems.movies.edit', $imdb->id)
                                    ->icon('film')
                                    ->target('_blank');
                        }
                    }

                    // visit imdb
                    $url = 'http://www.imdb.com/title/'.$file->imdbId;
                    if(!empty($file->season)) {
                        $url .= '/episodes?season='.$file->season;
                    }

                    $btns[] = Link::make('Visit IMDB')
                                ->href($url)
                                ->icon('link')
                                ->target('_blank');
                }

                $btns[] = Button::make('Mark as seen')
                            ->method('markAsWatched', ['fileId' => $file->id])
                            ->icon('eye');

                return DropDown::make()
                    ->icon('options-vertical')
                    ->list($btns);
            });

        return $tds;
    }
}
