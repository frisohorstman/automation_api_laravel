<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Files;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Layouts\Rows;

class FilesDeleteLayout extends Rows
{
    public $file;

    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        $this->file = $this->query->get('file');

        return [
            CheckBox::make('delete_file_record')
                ->title('Remove file')
                ->placeholder('From database. Gets re-imported if file exists upon scan.')
                ->checked(),

            CheckBox::make('delete_file_hdd')
                ->placeholder('From hard disk. Will be permanently removed!'),


            
            CheckBox::make('delete_folder_record')
                ->title('Permanently remove folder and associated files')
                ->placeholder('From database. Gets re-imported if file exists upon scan.'),

            CheckBox::make('delete_folder_hdd')
                ->placeholder('From hard disk. Will be permanently removed!'),
        ];
    }
}
