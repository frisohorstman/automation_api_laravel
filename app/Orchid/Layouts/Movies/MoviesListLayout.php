<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Movies;

// model
use App\Models\Imdb;

// screens
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class MoviesListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'movies';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [          
            TD::make('')
                ->width('100')
                ->render(function (Imdb $movie) {
                    $img = '<img src="'.$movie->poster.'" style="width: 100%;" />';
                    return '<a href="'.route('platform.systems.movies.edit', $movie->id).'">'.$img.'</a>';
                }),
            
            TD::make('title', __('Title'))
                ->sort()
                ->width('215')
                ->cantHide()
                ->filter(Input::make())
                ->render(function (Imdb $movie) {
                    $html  = Link::make($movie->title)->route('platform.systems.movies.edit', $movie->id)->style('font-size: 15px;');
                    $html .= '<div style="margin-left: 5px;">Score '.$movie->imdbRating.' out '.$movie->imdbVotes.' votes</div>';
                    $html .= '<div style="margin-left: 5px;">'.$movie->runtime.' - '.$movie->year.' -  Rated '.$movie->rated.'</div>';

                    return $html;
                }),

            TD::make('plot', __('Genre'))
                // ->sort()
                ->width('110')
                ->render(function (Imdb $movie) {
                    return implode('<br>', $movie->genre);
                }),

            TD::make('plot', __('Plot'))
                ->width('500')
                ->render(function (Imdb $movie) {
                    return $movie->plot;
                }),

            TD::make('created_at', __('Created'))
                ->sort()
                ->render(function (Imdb $movie) {
                    return $movie->created_at->diffForHumans();
                }),
        ];
    }
}
