<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Devices;

use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

use App\Models\Devices;

class DevicesListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'devices';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [          
            TD::make('Name')
                ->render(function (Devices $device) {
                    return Link::make($device->name)
                                ->route('platform.systems.devices.edit', $device->id);
                }),
            
            TD::make('Mac Address')
                ->width('200')
                ->render(function (Devices $device) {
                    return $device->mac;
                }),

            TD::make('')
                ->width('200')
                ->render(function (Devices $device) {
                    if($device->online) {
                        return '<span class="badge bg-success">Online</span>';
                    } else {
                        return '<span class="badge bg-danger">Offline</span>';
                    }
                }),

            TD::make('')
                ->align(TD::ALIGN_CENTER)
                ->width('30px')
                ->render(function (Devices $device) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.systems.devices.edit', $device->id)
                                ->icon('pencil'),
                            Button::make('Delete')
                                ->confirm('Device <i>'.$device->name.'</i> will be permanently removed.')
                                ->method('remove', ['deviceId' => $device->id])
                                ->icon('trash'),
                        ]);
                }),
        ];
    }
}
