<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Devices;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Layouts\Rows;

class ZwaveEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('device.zwave_port')
                ->type('text')
                ->max(255)
                ->title('Port'),

            Input::make('device.zwave_username')
                ->type('text')
                ->max(255)
                ->title('Username'),

            Input::make('device.zwave_password')
                ->type('password')
                ->max(255)
                ->title('Password')
                ->help('Please note; This password will be saved in the database in plain text.'),

        ];
    }
}
