<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Devices;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class DevicesEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('device.name')
                ->type('text')
                ->max(255)
                ->title(__('Name')),

            Input::make('device.mac')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Mac Address')),

            Select::make('device.type')
                ->title('Type')
                ->options([
                    null => null,
                    'zwave' => 'Z-wave controller',
                    'voice' => 'Jarvis voice recognition',
                    'media_server' => 'Media server connected to TV',
                ]),
        ];
    }
}
