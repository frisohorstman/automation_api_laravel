<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Voice;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class VoiceFunctionEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('function.comment')
                ->type('text')
                ->max(255)
                ->title(__('Comment'))
                ->help('Description of what will happen when this function is called.'),

            Input::make('function.module')
                ->type('text')
                ->max(255)
                ->title(__('Module'))
                ->help('A Python module.'),

            Input::make('function.class')
                ->type('text')
                ->max(255)
                ->title(__('Class'))
                ->help('The class within Python.'),

            Input::make('function.function')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Function'))
                ->help('A function within Python.'),

            Input::make('function.arg1')
                ->type('text')
                ->max(255)
                ->title(__('arg1'))
                ->help('First argument which will be passed to the function.'),

            Input::make('function.arg2')
                ->type('text')
                ->max(255)
                ->title(__('arg2'))
                ->help('Second argument which will be passed to the function.'),
        ];
    }
}
