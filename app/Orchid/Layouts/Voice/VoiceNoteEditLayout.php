<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Voice;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Layouts\Rows;

class VoiceNoteEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            TextArea::make('note.note')
                ->rows(6)
                ->title('Note')
                ->help('A note left by yourself relayed via the Jarvis system.'),
        ];
    }
}
