<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Voice;

use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

use App\Models\VoiceFunctions;

class VoiceFunctionsListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'functions';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [          
            TD::make('function')
                ->width(350)
                ->render(function (VoiceFunctions $function) {
                    $str = '';

                    if(!empty($function->module)) {
                        $str .= $function->module.'.';
                    }

                    if(!empty($function->class)) {
                        $str .= $function->class.'.';
                    }

                    $str .= $function->function;

                    if(!empty($function->arg1)) {
                        $str .= "('".$function->arg1;
                        if(!empty($function->arg2)) {
                            $str .= ",'".$function->arg2."'";
                        }
                        $str .= "')";
                    } else {
                        $str .= '()';
                    }

                    return Link::make($str)
                                ->route('platform.systems.voice.functions.edit', $function->id);
                }),

            TD::make('keywords', 'Keywords')
                // ->width('300')
                ->render(function (VoiceFunctions $function) {
                    $keywords = [];

                    foreach($function->keywords as $record) {
                        $keywords[] = $record->keyword;
                    }

                    return implode(', ', $keywords);
                }),

            TD::make('created_at', 'Created')
                ->width('120')
                ->render(function (VoiceFunctions $function) {
                    if(empty($function->created_at)) {
                        return false;
                    }
                    
                    return $function->created_at->diffForHumans();
                }),

            TD::make('')
                ->align(TD::ALIGN_CENTER)
                ->width('30px')
                ->render(function (VoiceFunctions $function) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.systems.voice.functions.edit', $function->id)
                                ->icon('pencil'),
                            Button::make('Delete')
                                ->confirm('Function & keywords will be permanently removed.')
                                ->method('remove', ['functionId' => $function->id])
                                ->icon('trash'),
                        ]);
                }),
        ];
    }
}
