<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\Voice;

use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

use App\Models\VoiceNotes;

class VoiceNotesListLayout extends Table
{
    /**
     * @var string
     */
    public $target = 'notes';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [          
            TD::make('function')
                ->render(function (VoiceNotes $note) {
                    return Link::make($note->note)
                                ->route('platform.systems.voice.notes.edit', $note->id);
                }),

            TD::make('created_at', 'Created')
                ->width('200')
                ->render(function (VoiceNotes $note) {
                    return $note->created_at->diffForHumans();
                }),

            TD::make('')
                ->align(TD::ALIGN_CENTER)
                ->width('30px')
                ->render(function (VoiceNotes $note) {
                    return DropDown::make()
                        ->icon('options-vertical')
                        ->list([
                            Link::make(__('Edit'))
                                ->route('platform.systems.voice.notes.edit', $note->id)
                                ->icon('pencil'),
                            Button::make('Delete')
                                ->confirm('Note will be permanently removed.')
                                ->method('remove', ['noteId' => $note->id])
                                ->icon('trash'),
                        ]);
                }),
        ];
    }
}
