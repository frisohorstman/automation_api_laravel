<?php

namespace App\Orchid\Screens\Voice;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Facades\Toast;

use App\Orchid\Layouts\Voice\VoiceFunctionsListLayout;

use App\Models\VoiceFunctions;

class VoiceFunctionsListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'functions' => VoiceFunctions::filters()->defaultSort('created_at', 'DESC')->paginate(),
        ];
    }
    

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Voice Functions';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.systems.voice.functions.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            VoiceFunctionsListLayout::class,
        ];
    }

    /**
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request)
    {
        $func = VoiceFunctions::find($request->get('functionId'));
        $func->delete();

        Toast::info(__('Voice function has been removed.'));
        return redirect()->route('platform.systems.voice.functions');
    }
}
