<?php

namespace App\Orchid\Screens\Voice;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Sight;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;

use App\Orchid\Layouts\Voice\VoiceFunctionEditLayout;

use App\Models\VoiceFunctions;
use App\Models\VoiceKeywords;

class VoiceFunctionEditScreen extends Screen
{
    public $function;
    
    /**
     * Query data.
     *
     * @param VoiceKeywords $keyword
     *
     * @return array
     */
    public function query(VoiceFunctions $function): iterable
    {
        return [
            'function' => $function,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Edit Voice Command';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->function->exists),
            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            VoiceFunctionEditLayout::class,
            Layout::view('Orchid/bootstrapTagsInput'),
        ];
    }


    /**
     * @param Request $request
     * @param VoiceFunctions $function
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, VoiceFunctions $function)
    {
        $function->comment = $request->input('function.comment');
        $function->module = $request->input('function.module');
        $function->class = $request->input('function.class');
        $function->function = $request->input('function.function');
        $function->arg1 = $request->input('function.arg1');
        $function->arg2 = $request->input('function.arg2');
        $function->save();

        // keywords
        $function->keywords()->delete(); // !confusion: why didnt schema onUpdate('cascade') didnt trigger?
        $recieved = explode(',', $_REQUEST['keywords']); // !warning: $_REQUEST here
        
        foreach($recieved as $k) {
            $keyword = new VoiceKeywords;
            $keyword->keyword = $k;
            $function->keywords()->save($keyword);
        }

        Toast::info(__('Saved voice function.'));
        return redirect()->route('platform.systems.voice.functions');
    }

    /**
     * @param Devices $device
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(VoiceFunctions $function)
    {
        $function->delete();

        Toast::info(__('Voice function has been removed.'));
        return redirect()->route('platform.systems.voice.functions');
    }
}


