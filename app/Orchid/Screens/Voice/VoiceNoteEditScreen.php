<?php

namespace App\Orchid\Screens\Voice;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Sight;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;

use App\Orchid\Layouts\Voice\VoiceNoteEditLayout;

use App\Models\VoiceNotes;

class VoiceNoteEditScreen extends Screen
{
    public $note;
    
    /**
     * Query data.
     *
     * @param VoiceKeywords $keyword
     *
     * @return array
     */
    public function query(VoiceNotes $note): iterable
    {
        return [
            'note' => $note,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Voice Note';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->note->exists),
            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            VoiceNoteEditLayout::class,
        ];
    }


    /**
     * @param Request $request
     * @param Devices $device
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, VoiceNotes $note)
    {
        $note->note = $request->input('note.note');
        $note->save();

        Toast::info(__('Saved note.'));
        return redirect()->route('platform.systems.voice.notes');
    }

    /**
     * @param Devices $device
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(VoiceNotes $note)
    {
        $note->delete();

        Toast::info(__('Note has been removed.'));
        return redirect()->route('platform.systems.voice.notes');
    }
}


