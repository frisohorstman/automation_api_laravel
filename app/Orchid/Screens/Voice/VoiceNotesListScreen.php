<?php

namespace App\Orchid\Screens\Voice;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Facades\Toast;

use App\Orchid\Layouts\Voice\VoiceNotesListLayout;

use App\Models\VoiceNotes;

class VoiceNotesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'notes' => VoiceNotes::paginate(),
        ];
    }
    

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Voice Notes';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.systems.voice.notes.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            VoiceNotesListLayout::class,
        ];
    }

    /**
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request)
    {
        $note = VoiceNotes::find($request->get('noteId'));
        $note->delete();

        Toast::info(__('Voice note has been removed.'));
        return redirect()->route('platform.systems.voice.notes');
    }
}
