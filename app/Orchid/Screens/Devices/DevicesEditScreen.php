<?php

namespace App\Orchid\Screens\Devices;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Sight;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;

use App\Orchid\Layouts\Devices\DevicesEditLayout;
use App\Orchid\Layouts\Devices\ZwaveEditLayout;

use App\Models\Devices;

class DevicesEditScreen extends Screen
{
    public $device;
    
    /**
     * Query data.
     *
     * @param Devices $device
     *
     * @return array
     */
    public function query(Devices $device): iterable
    {
        return [
            'device' => $device,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Device';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove')
                ->canSee($this->device->exists),
            Button::make(__('Save'))
                ->icon('check')
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $layouts = [
            DevicesEditLayout::class,

            Layout::block(ZwaveEditLayout::class)
                ->title('Z-wave')
                ->description('On what port does the Z-wave API webserver run?'),
        ];

        if(!empty($device->online)) {
            $layouts[] = Layout::legend('device', [
                Sight::make('online')->render(function (Devices $device) {
                    if($device->online) {
                        return '<span class="badge bg-success">Online</span>';
                    } else {
                        return '<span class="badge bg-danger">Offline</span>';
                    }
                }),
                Sight::make('ip', 'IP Address'),
                Sight::make('cpu', 'Processor'),
                Sight::make('memory')->render(function (Devices $device) {
                    return str_replace(',', '<br>', $device->memory);
                }),
                Sight::make('updated_at', 'Last updated')->render(function (Devices $device) {
                    if($device->exists) {
                        return $device->updated_at->diffForHumans();
                    }
                }),
            ])->title('Device information');
        }
            
        return $layouts;
    }


    /**
     * @param Request $request
     * @param Devices $device
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, Devices $device)
    {
        $device->name = $request->input('device.name');
        $device->mac = $request->input('device.mac');
        $device->type = $request->input('device.type');

        // if $_REQUEST[type] == 'zwave' then ... else null
        $device->zwave_port = ($request->input('device.type') == 'zwave') ? $request->input('device.zwave_port') : null;
        $device->zwave_username = ($request->input('device.type') == 'zwave') ? $request->input('device.zwave_username') : null;
        $device->zwave_password = ($request->input('device.type') == 'zwave') ? $request->input('device.zwave_password') : null;
        $device->save();

        Toast::info(__('Device was saved.'));

        return redirect()->route('platform.systems.devices');
    }

    /**
     * @param Devices $device
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Devices $device)
    {
        $device->delete();

        Toast::info(__('Device has been removed.'));
        return redirect()->route('platform.systems.devices');
    }
}


