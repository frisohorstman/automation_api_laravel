<?php

namespace App\Orchid\Screens\Devices;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Facades\Toast;

use App\Orchid\Layouts\Devices\DevicesListLayout;
use App\Models\Devices;

class DevicesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'devices' => Devices::paginate(),
        ];
    }
    

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Devices';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->href(route('platform.systems.devices.create')),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            DevicesListLayout::class,
        ];
    }

    /**
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Request $request)
    {
        $device = Devices::find($request->get('deviceId'));
        $device->delete();

        Toast::info(__('Device has been removed.'));
        return redirect()->route('platform.systems.devices');
    }
}
