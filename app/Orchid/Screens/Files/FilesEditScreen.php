<?php

namespace App\Orchid\Screens\Files;

use Illuminate\Http\Request;

// orchid
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;

// movies
use App\Orchid\Layouts\Files\FilesEditLayout;
use App\Orchid\Screens\Files\FileTypeDetailsTable;

// model
use App\Models\Files;
use App\Models\Imdb;

class FilesEditScreen extends Screen
{
    public $file;
    
    /**
     * Query data.
     *
     * @param Imdb $movie
     *
     * @return array
     */
    public function query(Files $file): iterable
    {
        return [
            'file' => $file,
            'videoFile' => $file,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'File Information';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Link::make('Delete')
                ->icon('trash')
                ->type(Color::SECONDARY())
                ->route('platform.systems.files.delete', $this->file->id),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $layouts = [
            Layout::legend('fileInfo', [
                Sight::make('path')->render(function () {
                    return $this->file->path.'/';
                }),
                Sight::make('folder', 'Folder')->render(function () {
                    return $this->file->folderName;
                }),
                Sight::make('filename')->render(function () {
                    return $this->file->filename;
                }),
                Sight::make('originalFilename', 'Original filename')->render(function () {
                    return $this->file->originalFilename;
                }),
                Sight::make('cleanName', 'Clean name')->render(function () {
                    return $this->file->cleanName;
                }),
                Sight::make('watched', 'Watched')->render(function () {
                    if($this->file->watched) {
                        return 'Seen';
                    } else {
                        return 'Unseen';
                    }
                }),
                Sight::make('searchedOmdbApi', 'OMDB API')->render(function () {
                    $txt = '';
                    if($this->file->searchedOmdbApi) {
                        $txt .= 'Was searched';
                    } else {
                        $txt .= 'Not searched';
                    }

                    $txt .= Link::make('Search')
                                ->class('btn float-start me-2 mt-3 text-light')
                                ->icon('magnifier')
                                ->type(Color::PRIMARY())
                                ->route('platform.systems.omdb.search', ['fileId' => $this->file->id]);
                    
                    return $txt;
                }),
                Sight::make('imdbId', 'IMDB ID')->render(function () {
                    return $this->file->imdbId ? $this->file->imdbId : '-';
                }),
                Sight::make('created')->render(function () {
                    return $this->file->created_at->diffForHumans();
                }),
                Sight::make('imported')->render(function () {
                    return $this->file->imported_at->diffForHumans();
                }),
                Sight::make('md5')->render(function () {
                    return $this->file->md5;
                }),
                Sight::make('imdbId', '')->render(function () {
                    if(empty($this->file->imdbId)) {
                        return '';
                    }

                    $imdb = Imdb::firstWhere('imdbId', $this->file->imdbId);

                    $url = 'http://www.imdb.com/title/'.$this->file->imdbId;

                    if(!empty($file->season)) {
                        $url .= '/episodes?season='.$this->file->season;
                    }
                    
                    return  Link::make('Visit IMDB')->class('btn float-start text-light me-2')
                                ->type(Color::PRIMARY())
                                ->icon('link')
                                ->href($url)
                                ->target('_blank').
                            Link::make('Movie Page')->class('btn float-start text-light me-2')
                                ->type(Color::PRIMARY())
                                ->icon('film')
                                ->route('platform.systems.movies.edit', $imdb->id);
                }),
            ]),
        ];


        // file type information
        $title = false;
        if($this->file->isVideo) {
            $title = 'Video information';
        }

        if($this->file->isImage) {
            $title = 'Image information';
        }

        // file type details
        $table = new FileTypeDetailsTable($this->file);
        $layouts[] = $table->layout();

        // video
        if($this->file->isVideo) {
            $layouts[] = Layout::view('Orchid/videoPlayer');
        }

        // image
        if($this->file->isImage) {
            $layouts[] = Layout::view('Orchid/image');
        }

        return $layouts;
    }

    // /**
    //  * @param Files $file
    //  *
    //  * @throws \Exception
    //  *
    //  * @return \Illuminate\Http\RedirectResponse
    //  */
    // public function remove(Files $file)
    // {
    //     $file->delete();

    //     Toast::info(__('File was removed from database.'));

    //     return redirect()->route('platform.systems.files');
    // }
}


