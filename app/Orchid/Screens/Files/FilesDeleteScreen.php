<?php

namespace App\Orchid\Screens\Files;

use Illuminate\Http\Request;

// orchid
use Orchid\Screen\Screen;
use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;
use Orchid\Screen\Fields\CheckBox;

use Orchid\Support\Facades\Alert;

use App\Orchid\Layouts\Files\FilesDeleteLayout;

// model
use App\Models\Files;

class FilesDeleteScreen extends Screen
{
    public $file;
    
    /**
     * Query data.
     *
     * @param Imdb $movie
     *
     * @return array
     */
    public function query(Files $file): iterable
    {
        return [
            'file' => $file,
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Delete File';
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $description  = 'Make sure the checkboxes of the things you want deleted are checked.';
        $description .= '<br><br><b>Filename</b><br>'.$this->file->filename;

        $description .= '<br><br><b>Path</b><br>'.$this->file->path;

        $description .= '<br><br><b>Folder</b><br>';
        $description .= '<ul class="recursive_folder"><li>'.$this->file->folderName.'/ </li>';
        $description .= '<li>'.$this->dirToUnorderedList($this->file->path).'</li></ul>';
        
        $layouts = [
            Layout::block(FilesDeleteLayout::class)
                ->title(__('Are you sure?'))
                ->description($description)
                ->commands(
                    Button::make(__('Delete'))
                        ->type(Color::DANGER())
                        ->icon('trash')
                        ->canSee(true)
                        ->method('removeFile')
                ),
        ];

        return $layouts;
    }


    /**
     * return string;
     * 
     * make unordered list of directory (recursively)
     */
    public function dirToUnorderedList($root, $return_ul = false)
    {
        $str = '';
        $dir = scandir($root);

        // scan it recursively to get the files inside directories
        foreach($dir as $filename) {
            // skip the unix dots
            if($filename == '.' || $filename == '..') {
                continue;
            }

            $filepath = $root.'/'.$filename;

            if(is_file($filepath)) {
                $file = Files::where('filepath', $filepath)->first();
                $str .= '<li><a href="'.route('platform.systems.files.edit', $file->id).'">'.basename($filepath).'</a></li>';
            } else {
                // dir
                $str .= '<li>'.basename($filepath).'/</li>';
                $str .= '<li>'.$this->dirToUnorderedList($filepath).'</li>';
            }
        }

        return '<ul>'.$str.'</ul>';
    }

    /**
     * return boolean;
     * 
     */
    public function recursiveRemove($root, $request)
    {
        $allSuccess = true;
        $dir = scandir($root);

        // scan it recursively to get the files inside directories
        foreach($dir as $filename) {
            // skip the unix dots
            if($filename == '.' || $filename == '..') {
                continue;
            }

            $filepath = $root.'/'.$filename;

            if(is_file($filepath)) {
                // file
                if($request->input('delete_folder_hdd') == 'on') {
                    $success = unlink($filepath);

                    if($success == false) {
                        $allSuccess = false;
                    }
                }

                if($request->input('delete_folder_record') == 'on') {
                    // find file in database
                    $found = Files::firstWhere('filepath', $filepath);
                    if(!empty($found)) {
                        if($found->delete() == false) {
                            $allSuccess = false;
                        }
                    }
                }
            } else {
                // dir
                $success = $this->recursiveRemove($filepath, $request);

                if($success == false) {
                    $allSuccess = false;
                } else {
                    rmdir($filepath);
                }
            }
        }

        return $allSuccess;
    }

    /**
     * @param Files $file
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeFile(Request $request, Files $file)
    {
        // folder
        if($request->input('delete_folder_record') == 'on' || $request->input('delete_folder_hdd') == 'on') {
            $success = $this->recursiveRemove($file->path, $request);

            $what = 'hard disk.';
            if($request->input('delete_folder_record') == 'on') {
                $what = 'database.';
            }

            if($request->input('delete_folder_record') == 'on' && $request->input('delete_folder_hdd') == 'on') {
                $what = 'database and hard disk.';
            }

            if($success) {
                Alert::success('Successfully removed folder from '.$what);
            } else {
                Alert::error('Something failed while removing folder from '.$what);
            }

            return redirect()->route('platform.systems.files');
        }

        // file
        $what = 'hard disk.';
        if($request->input('delete_file_record') == 'on') {
            $what = 'database.';
        }

        if($request->input('delete_file_record') == 'on' && $request->input('delete_file_hdd') == 'on') {
            $what = 'database and hard disk.';
        }

        if($request->input('delete_file_hdd') == 'on') {
            $success = unlink($file->filepath);
        }

        if($request->input('delete_file_record') == 'on') {
            $success = $file->delete();
        }

        if($success) {
            Alert::success('Successfully removed file from '.$what);
        } else {
            Alert::error('Something failed while removing file from '.$what);
        }

        return redirect()->route('platform.systems.files');
    }
}


