<?php

namespace App\Orchid\Screens\Files;

use Orchid\Screen\Sight;
use Orchid\Support\Facades\Layout;

class FileTypeDetailsTable {
    private $file;
  
    public function __construct($file) {
      $this->file = $file;
    }

    /**
     * A simple table with information about the file 
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout()
    {
        $table = [];

        // runtime
        if(isset($this->file->id3['playtime_seconds'])) {
            $table[] = Sight::make('runtime')->render(function () {
                $hours = gmdate("G", $this->file->id3['playtime_seconds']);
                $minutes = gmdate("i", $this->file->id3['playtime_seconds']);
                
                return $hours.' hour(s) and '.$minutes.' minutes';
            });
        }

        // extension
        $table[] = Sight::make('extension')->render(function () {
            return $this->file->extension;
        });

        // size
        $table[] = Sight::make('size')->render(function () {
            return $this->file->sizeHuman;
        });

        // bitrate
        if(isset($this->file->id3['bitrate'])) {
            $table[] = Sight::make('bitrate')->render(function () {
                $mbs = round($this->file->id3['bitrate'] / 1000 / 1000, 2);
                return $mbs.' mb/s';
            });
        }

        // framerate
        if(isset($this->file->id3['video']['frame_rate'])) {
            $table[] = Sight::make('framerate')->render(function () {
                return round($this->file->id3['video']['frame_rate'], 2).'/s';
            });
        }

        // resolution
        if(isset($this->file->id3['video']['resolution_x'])) {
            $table[] = Sight::make('resolution')->render(function () {
                return $this->file->id3['video']['resolution_x'].'x'.$this->file->id3['video']['resolution_y'];
            });
        }

        // codec
        if($this->file->isVideo) {
            // codec thats set when imported. is interpeted via TorrentName
            if(isset($this->file->codec)) {
                $table[] = Sight::make('codec')->render(function () {
                    return $this->file->codec;
                });
            }

            // dataformat
            if(isset($this->file->id3['video']['dataformat'])) {
                $table[] = Sight::make('format')->render(function () {
                    return ucfirst($this->file->id3['video']['dataformat']);
                });
            }            

            // codec via fourcc_lookup
            if(isset($this->file->id3['video']['fourcc_lookup'])) {
                $table[] = Sight::make('codec')->render(function () {
                    return $this->file->id3['video']['fourcc_lookup'];
                });
            }

            // audio
            if(isset($this->file->id3['audio'])) {
                $table[] = Sight::make('audio')->render(function () {
                    $txt = '';

                    foreach($this->file->id3['audio']['streams'] as $stream) {
                        if(isset($stream['language'])) {
                            $txt .= ucfirst($stream['language']).', ';
                        }
                        $txt .= 'format: '.$stream['dataformat'].', sample rate: '.$stream['sample_rate'].', '.$stream['channels'].' channels<br>';
                    }

                    return ucfirst($txt);
                });
            }
        }

        // ripSource
        if($this->file->isVideo && !empty($this->file->ripSource)) {
            $table[] = Sight::make('ripSource', 'RIP Source')->render(function () {
                return $this->file->ripSource;
            });
        }

        // season / episode
        if($this->file->isVideo && !empty($this->file->season)) {
            $table[] = Sight::make('seasonEpisode', 'Season & Episode')->render(function () {
                $str  = 'S'.sprintf("%02d", $this->file->season);
                $str .= ' E'.sprintf("%02d", $this->file->episode);

                return $str;
            });
        }

        // file type information title
        $title = false;
        if($this->file->isVideo) {
            $title = 'Video information';
        }

        if($this->file->isImage) {
            $title = 'Image information';
        }

        return Layout::legend('fileType', $table)->title($title);
    }
}
