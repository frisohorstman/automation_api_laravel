<?php

namespace App\Orchid\Screens\Files;

use Illuminate\Http\Request;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Screen;

use App\Orchid\Layouts\Files\FilesListLayout;
use App\Models\Files;

class FilesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query() : array
    {
        return [
            'files' => Files::filters()->defaultSort('created_at', 'DESC')->paginate(),
        ];
    }
    

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Files';
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            FilesListLayout::class,
        ];
    }

    /**
     * marks a movie as watched
     */
    public function markAsWatched(Request $request)
    {
        $file = Files::find($request->get('fileId'));
        $file->watched = true;
        $file->save();

        Toast::info('File is marked as watched.');
    }
}
