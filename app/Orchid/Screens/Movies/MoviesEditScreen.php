<?php

namespace App\Orchid\Screens\Movies;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Screen\Sight;

use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;

use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\CheckBox;

use App\Orchid\Layouts\Files\FilesListLayout;
use App\Orchid\Screens\Files\FileTypeDetailsTable;

use App\Models\Imdb;
use App\Models\Files;

class MoviesEditScreen extends Screen
{
    public $movie;
    public $videoFile;
    
    /**
     * Query data.
     *
     * @param Imdb $movie
     *
     * @return array
     */
    public function query(Imdb $movie): iterable
    {
        return [
            'movie' => $movie,
            'files' => Files::where('imdbId', $movie->imdbId)
                            ->filters()
                            ->orderBy('season', 'DESC')
                            ->orderBy('episode', 'DESC')
                            ->paginate(),
                            // ->get(),
            'videoFile' => Files::where('imdbId', $movie->imdbId)
                            ->where('isVideo', true)
                            ->first(),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Movie Edit';
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Remove'))
                ->icon('trash')
                ->method('remove'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        $sights = [
            Sight::make('title')->render(function (Imdb $imdb) {
                return '<a href="https://www.imdb.com/title/'.$imdb->imdbId.'" title="Click to visit IMDB" target="_blank" style="text-decoration: underline;">'.$imdb->title.'</a>';
            }),
            Sight::make('year', 'Year')->render(function(Imdb $imdb) {
                return $imdb->year.' - '.$imdb->rated.' - '.$imdb->runtime;
            }),
            Sight::make('plot'),
            Sight::make('imdbRating', 'IMDB Rating')->render(function (Imdb $imdb) {
                return $imdb->imdbRating.' (Out of '.$imdb->imdbVotes.' votes)';
            }),
            Sight::make('released'),
            Sight::make('genre')->render(function (Imdb $imdb) {
                $btns = '';
                foreach($imdb->genre as $genre) {
                    $btns .= Link::make($genre)->class('btn float-start me-2')->type(Color::DARK());
                }
                
                return $btns;
            }),
        ];
        
        if($this->movie->director) {
            $sights[] = Sight::make('director')->render(function (Imdb $imdb) {
                $arr = [];
                foreach($imdb->director as $name) {
                    $arr[] = '<a href="https://www.imdb.com/search/name/?name='.$name.'" target="_blank">'.$name.'</a>';
                }
                
                return implode(', ', $arr);
            });
        }

        if($this->movie->writer) {
            $sights[] = Sight::make('writer')->render(function (Imdb $imdb) {
                $arr = [];
                foreach($imdb->writer as $name) {
                    $arr[] = '<a href="https://www.imdb.com/search/name/?name='.$name.'" target="_blank">'.$name.'</a>';
                }
                
                return implode(', ', $arr);
            });
        }

        if($this->movie->actors) {
            $sights[] = Sight::make('actors')->render(function (Imdb $imdb) {
                $arr = [];
                foreach($imdb->actors as $name) {
                    $arr[] = '<a href="https://www.imdb.com/search/name/?name='.$name.'" target="_blank">'.$name.'</a>';
                }
                
                return implode(', ', $arr);
            });
        }

        if($this->movie->language) {
            $sights[] = Sight::make('language')->render(function (Imdb $imdb) {
                $btns = '';
                foreach($imdb->language as $language) {
                    $btns .= Link::make($language)->class('btn btn-secondary float-start me-2')->type(COLOR::DARK());
                }
                
                return $btns;
            });
        }

        if($this->movie->country) {
            $sights[] = Sight::make('country')->render(function (Imdb $imdb) {
                $btns = '';
                foreach($imdb->country as $country) {
                    $btns .= Link::make($country)->class('btn float-start me-2')->type(Color::DARK());
                }
                
                return $btns;
            });
        }

        if($this->movie->ratings) {
            $sights[] = Sight::make('ratings')->render(function (Imdb $imdb) {
                $txt = '';
                foreach($imdb->ratings as $r) {
                    $txt .= '<span style="width: 50px; padding-right: 5px; text-align: center; display: inline-block;">'.$r['Value'].'</span>'.$r['Source'].'<br>';
                }
                
                return $txt;
            });
        }

        if($this->movie->awards) {
            $sights[] = Sight::make('awards');
        }

        if($this->movie->dvd) {
            $sights[] = Sight::make('dvd', 'DVD Released');
        }

        if($this->movie->boxOffice) {
            $sights[] = Sight::make('boxOffice', 'Box Office');
        }

        if($this->movie->production) {
            $sights[] = Sight::make('production');
        }

        if($this->movie->website) {
            $sights[] = Sight::make('website');
        }

        if($this->movie->created_at) {
            $sights[] = Sight::make('created_at', 'Created')->render(function (Imdb $imdb) {
                return $imdb->created_at->diffForHumans();
            });
        }

        if($this->movie->imdbId) {
            $sights[] = Sight::make('imdbId', '')->render(function (Imdb $imdb) {
                $imdb = Imdb::firstWhere('imdbId', $imdb->imdbId);

                return  Link::make('Visit IMDB')->class('btn float-start me-2')
                            ->type(Color::PRIMARY())
                            ->href('http://www.imdb.com/title/'.$imdb->imdbId)
                            ->icon('link')
                            ->target('_blank');
            });
        }
        
        $layouts = [
            Layout::rows([
                CheckBox::make('checkbox')
                    ->title('Do you want to share this movie with other users via API?')
                    ->placeholder('Yes, make available via API.'),
            ]),
            Layout::legend('movie', $sights)->title('Movie information'),
        ];
        
        if($this->videoFile->type == 'movie') {
            // file type details
            $table = new FileTypeDetailsTable($this->videoFile);
            $layouts[] = $table->layout();
            
            // video player
            $layouts[] = Layout::view('Orchid/videoPlayer');
        }

        // files @todo: a title would be nice
        $layouts[] = FilesListLayout::class;

        return $layouts;
    }


    /**
     * @param Request $request
     * @param Imdb  $movie
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request, Imdb $movie)
    {
        $movie->title = $request->input('movie.title');
        $movie->save();

        Toast::info(__('Movie was saved'));

        return redirect()->route('platform.systems.movies');
    }

    /**
     * @param Imdb $movie
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Imdb $movie)
    {
        $movie->delete();

        Toast::info(__('Movie was removed from database.'));

        return redirect()->route('platform.systems.movies');
    }

    /**
     * marks a movie as watched
     */
    public function markAsWatched(Request $request)
    {
        $file = Files::find($request->get('fileId'));
        $file->watched = true;
        $file->save();

        Toast::info('File is marked as watched.');
    }
}


