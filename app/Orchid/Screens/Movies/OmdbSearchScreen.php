<?php

namespace App\Orchid\Screens\Movies;

use Illuminate\Http\Request;

use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;
use Orchid\Screen\Fields\Input;

use App\Classes\OmdbApi;
use App\Models\Imdb;
use App\Models\Files;

class OmdbSearchScreen extends Screen
{   
    public $file;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Request $request): iterable
    {
        return [
            'file' => Files::where('id', $request->get('fileId'))->first(),
        ];
    }

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Internet Movie Datababase Search';
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {   
        $layouts = [
            Layout::rows([
                Input::make('search')
                    ->title('Search movie or series name')
                    ->value($this->file->cleanName),

                Input::make('year')
                    ->title('Year')
                    ->value($this->file->year),

                // hidden fields
                Input::make('fileId')
                    ->value($this->file->id)
                    ->hidden(),

                Input::make('update')
                    ->value('one')
                    ->hidden(),

                Input::make('cleanName')
                    ->value($this->file->cleanName)
                    ->hidden(),

                // button, gets overriden via javascript
                Button::make('Search')
                    ->icon('magnifier')
                    ->class('btn omdb-search-btn float-start me-2')
                    ->type(Color::SECONDARY()),

                Button::make('save')
                    ->class('btn btn-save')
                    ->method('save'),
            ]),
        ];

        $layouts[] = Layout::view('Orchid/omdbSearchResult');

        return $layouts;
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $year = false;
        if(!empty($request->input('year'))) {
            $year = $request->input('year');
        }

        $api = new OmdbApi();
        $omdb = $api->search($request->input('search'), $year);

        if($omdb === false) {
            Toast::error('Getting OMDB whilest saving failed.');
            return redirect()->route('platform.systems.files.edit', $request->input('fileId'));
        }

        // import imdb movie
        Imdb::saveOmdbMovie($omdb);

        // update file(s) with new imdb id
        $one = Files::where('id', $request->input('fileId'))->first();

        $saved = false;
        if($request->input('update') == 'one') {
            $one->imdbId = $omdb['imdbID'];
            $one->searchedOmdbApi = true;
            $saved = $one->save();
        }

        if($request->input('update') == 'all') {
            $saved = true;
            $files = Files::where('cleanName', 'ILIKE', $one->cleanName)->get();

            foreach($files as $file) {
                $file->imdbId = $omdb['imdbID'];
                $success = $file->save();

                if(!$success) {
                    $saved = false;
                }
            }
        }

        if($saved) {
            Toast::info('New IMDB ID(\'s) assigned.');
        } else {
            Toast::error('Could not update IMDB ID(\'s).');
        }

        return redirect()->route('platform.systems.files.edit', $request->input('fileId'));
    }
}


