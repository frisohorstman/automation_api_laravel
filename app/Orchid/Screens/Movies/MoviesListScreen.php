<?php

namespace App\Orchid\Screens\Movies;

use Orchid\Screen\Screen;

use Orchid\Screen\Actions\Button;

use App\Orchid\Layouts\Movies\MoviesListLayout;

// models
use App\Models\Imdb;

class MoviesListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query() : array
    {
        if(\Request::route()->getName() == 'platform.systems.movies') {
            $movies = Imdb::where('type', 'movie')->filters()->paginate();
        }

        if(\Request::route()->getName() == 'platform.systems.series') {
            $movies = Imdb::where('type', 'series')->filters()->paginate();
        }

        return [
            'movies' => $movies,
        ];
    }
    

    /**
     * Display header name.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        if(\Request::route()->getName() == 'platform.systems.series') {
            return 'Series';
        }

        if(\Request::route()->getName() == 'platform.systems.movies') {
            return 'Movies';
        }
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make('Scan for files')
                ->icon('eye')
                ->method('scan'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): iterable
    {
        return [
            MoviesListLayout::class,
        ];
    }

    public function scan()
    {
        echo "yo, scan die shizzle dan..";
        die();
    }
}
