<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Screen\Actions\Menu;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // additional custom css to fix some orchid flaws or shortcomings
        $dashboard->registerResource('stylesheets', '/cms/css/custom.css');
        $dashboard->registerResource('scripts', '/cms/js/omdb.js');
        $dashboard->registerResource('scripts', '/cms/js/devices.js');
    }

    /**
     * @return Menu[]
     */
    public function registerMainMenu(): array
    {
        return [
            Menu::make('Example screen')
                ->icon('monitor')
                ->route('platform.example')
                ->title('Navigation')
                ->badge(function () {
                    return 6;
                }),

            Menu::make('Basic Elements')
                ->title('Form controls')
                ->icon('note')
                ->route('platform.example.fields'),

            Menu::make('Advanced Elements')
                ->icon('briefcase')
                ->route('platform.example.advanced'),


            //////////////// friso's stuff
            // media
            Menu::make(__('Movies'))
                ->icon('film')
                ->route('platform.systems.movies')
                ->permission('platform.systems.movies')
                ->title(__('Media')),

            Menu::make(__('Series'))
                ->icon('disc')
                ->route('platform.systems.series')
                ->permission('platform.systems.movies'),

            Menu::make(__('Files'))
                ->icon('new-doc')
                ->route('platform.systems.files')
                ->permission('platform.systems.files'),

            // voice
            Menu::make(__('Functions'))
                ->icon('code')
                ->route('platform.systems.voice.functions')
                // ->permission('platform.systems.voice.keywords')
                ->title(__('Voice')),

            Menu::make(__('Notes'))
                ->icon('bubble')
                ->route('platform.systems.voice.notes'),
                // ->permission('platform.systems.voice.keywords')

            // other
            Menu::make(__('Devices'))
                ->icon('screen-smartphone')
                ->route('platform.systems.devices')
                // ->permission('platform.systems.movies')
                ->title(__('Other')),

            // system users & roles
            Menu::make(__('Users'))
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->title(__('Access rights')),

            Menu::make(__('Roles'))
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles'),
        ];
    }

    /**
     * @return Menu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            Menu::make('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('System'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users'))

                // friso's stuff
                ->addPermission('platform.systems.movies', __('Movies'))
                ->addPermission('platform.systems.files', __('Files')),
        ];
    }
}
