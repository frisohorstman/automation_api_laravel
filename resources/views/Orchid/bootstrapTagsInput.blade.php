
<link href="/libraries/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
<script src="/libraries/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<style>
.bootstrap-tagsinput {
    -webkit-appearance:none;
    -moz-appearance:none;
    background:#fff none;
    border:1px solid rgba(28,43,54,.1);
    border-radius:2px;
    max-width: 600px;
    box-shadow:none;
    color:#1c2b36;
    line-height:normal;
    outline:0;
    transition:all .12s ease;
    transition:background .2s linear 0s;
    vertical-align:middle;
}

    .bootstrap-tagsinput .tag {
        background-color: #0d6efd;
        border-radius: 3px;
        padding: 3px;
        padding-left: 5px;
        padding-right: 5px;
        margin: 3px;
        margin-right: 0px;
        font-size: 14px;
        float: left;
        font-size: 12px;
        font-weight: normal;
    }
</style>

<fieldset class="mb-3" data-async="">
    <div class="bg-white rounded shadow-sm p-4 py-4 d-flex flex-column">
        <div class="form-group">
            <label for="keywords" class="form-label">
                Keywords
            </label>
            <div data-controller="input" data-input-mask="">
                <input type="text" value="{{ $function->keywords->pluck('keyword')->implode(', ') }}" name="keywords" id="keywords" data-role="tagsinput" class="custom-bootstrap-bg" />
            </div>
            <small class="form-text text-muted">Keywords that will be listened for.</small>
        </div>
    </div>
</fieldset>