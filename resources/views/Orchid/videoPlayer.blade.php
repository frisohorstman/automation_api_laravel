@if($videoFile->contentType == 'video/x-matroska' || $videoFile->codec == 'x265')
    <div style="color: red; margin-bottom: 10px;">Embedded playback is not supported.</div>
@else
    <video style="width: 100%; margin-bottom: 10px;" id="video_file" controls>
        <source src="{{ $videoFile->apiDownloadUrl }}" type="{{ $videoFile->contentType }}">
        Your browser does not support the video tag.
    </video>
@endif