<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

use App\Orchid\Screens\Examples\ExampleCardsScreen;
use App\Orchid\Screens\Examples\ExampleChartsScreen;
use App\Orchid\Screens\Examples\ExampleFieldsAdvancedScreen;
use App\Orchid\Screens\Examples\ExampleFieldsScreen;
use App\Orchid\Screens\Examples\ExampleLayoutsScreen;
use App\Orchid\Screens\Examples\ExampleScreen;
use App\Orchid\Screens\Examples\ExampleTextEditorsScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;

// movies
use App\Orchid\Screens\Movies\MoviesListScreen;
use App\Orchid\Screens\Movies\MoviesEditScreen;
use App\Orchid\Screens\Movies\OmdbSearchScreen;

// files
use App\Orchid\Screens\Files\FilesListScreen;
use App\Orchid\Screens\Files\FilesEditScreen;
use App\Orchid\Screens\Files\FilesDeleteScreen;

// devices
use App\Orchid\Screens\Devices\DevicesListScreen;
use App\Orchid\Screens\Devices\DevicesEditScreen;

// voice
use App\Orchid\Screens\Voice\VoiceFunctionsListScreen;
use App\Orchid\Screens\Voice\VoiceFunctionEditScreen;
use App\Orchid\Screens\Voice\VoiceNotesListScreen;
use App\Orchid\Screens\Voice\VoiceNoteEditScreen;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
// Route::screen('something', PlatformScreen::class)->name('platform.index');
Route::screen('main', PlatformScreen::class)->name('platform.main');

// Route::get('/', [PlatformScreen::class, 'index'])
//     ->name('index')
//     ->breadcrumbs(function (Trail $trail) {
//         return $trail->push(__('Home'), route('platform.index'));
//     });

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit')
    ->breadcrumbs(function (Trail $trail, $user) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('User'), route('platform.systems.users.edit', $user));
    });

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });


///////////////////////////////////
// Movies


// Platform > System > Series
Route::screen('series', MoviesListScreen::class)
    ->name('platform.systems.series')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Series'), route('platform.systems.series'));
    });

// Platform > System > Movies
Route::screen('movies', MoviesListScreen::class)
    ->name('platform.systems.movies')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Movies'), route('platform.systems.movies'));
    });

// Platform > System > Movies > Edit
Route::screen('movies/{movie}/edit', MoviesEditScreen::class)
    ->name('platform.systems.movies.edit')
    ->breadcrumbs(function (Trail $trail, $movie) {
        return $trail
            ->parent('platform.systems.movies')
            ->push(__('Movie Edit'), route('platform.systems.movies.edit', $movie));
    });

// Platform > System > Omdb > Search
Route::screen('omdb/search', OmdbSearchScreen::class)
    ->name('platform.systems.omdb.search');


///////////////////////////////////
// Files
Route::screen('files', FilesListScreen::class)
    ->name('platform.systems.files')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Files'), route('platform.systems.files'));
    });

// Platform > System > Files > Edit
Route::screen('files/{fileId}/edit', FilesEditScreen::class)
    ->name('platform.systems.files.edit')
    ->breadcrumbs(function (Trail $trail, $fileId) {
        return $trail
            ->parent('platform.systems.files')
            ->push(__('File Information'), route('platform.systems.files.edit', $fileId));
    });

// Platform > System > Files > Delete
Route::screen('files/{fileId}/delete', FilesDeleteScreen::class)
    ->name('platform.systems.files.delete')
    ->breadcrumbs(function (Trail $trail, $fileId) {
        return $trail
            ->parent('platform.systems.files')
            ->push(__('Delete File'), route('platform.systems.files.delete', $fileId));
    });

///////////////////////////////////
// Devices
Route::screen('devices', DevicesListScreen::class)
    ->name('platform.systems.devices')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Devices'), route('platform.systems.devices'));
    });

// Platform > System > Devices > Create
Route::screen('devices/create', DevicesEditScreen::class)
    ->name('platform.systems.devices.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.devices')
            ->push(__('Create'), route('platform.systems.devices.create'));
    });

// Platform > System > Devices > Edit
Route::screen('devices/{deviceId}/edit', DevicesEditScreen::class)
    ->name('platform.systems.devices.edit')
    ->breadcrumbs(function (Trail $trail, $deviceId) {
        return $trail
            ->parent('platform.systems.devices')
            ->push(__('Edit'), route('platform.systems.devices.edit', $deviceId));
    });

///////////////////////////////////
// Voice 

// Platform > System > Voice Functions > Edit
Route::screen('voice/functions', VoiceFunctionsListScreen::class)
    ->name('platform.systems.voice.functions')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Functions'), route('platform.systems.voice.functions'));
    });

// Platform > System > Voice Functions > Create
Route::screen('voice/functions/create', VoiceFunctionEditScreen::class)
    ->name('platform.systems.voice.functions.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.voice.functions')
            ->push(__('Create'), route('platform.systems.voice.functions.create'));
    });

// Platform > System > Voice Functions > Edit
Route::screen('voice/functions/{functionId}/edit', VoiceFunctionEditScreen::class)
    ->name('platform.systems.voice.functions.edit')
    ->breadcrumbs(function (Trail $trail, $functionId) {
        return $trail
            ->parent('platform.systems.voice.functions')
            ->push(__('Edit'), route('platform.systems.voice.functions.edit', $functionId));
    });

// Platform > System > Voice Notes > Edit
Route::screen('voice/notes', VoiceNotesListScreen::class)
    ->name('platform.systems.voice.notes')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Notes'), route('platform.systems.voice.notes'));
    });

// Platform > System > Voice Notes > Create
Route::screen('voice/notes/create', VoiceNoteEditScreen::class)
    ->name('platform.systems.voice.notes.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.voice.notes')
            ->push(__('Create'), route('platform.systems.voice.notes.create'));
    });

// Platform > System > Voice Notes > Edit
Route::screen('voice/notes/{noteId}/edit', VoiceNoteEditScreen::class)
    ->name('platform.systems.voice.notes.edit')
    ->breadcrumbs(function (Trail $trail, $noteId) {
        return $trail
            ->parent('platform.systems.voice.notes')
            ->push(__('Edit'), route('platform.systems.voice.notes.edit', $noteId));
    });




///////////////////////////////////
// Platform > System > Roles > Role
Route::screen('roles/{role}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Example...
Route::screen('example', ExampleScreen::class)
    ->name('platform.example')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Example screen');
    });

Route::screen('example-fields', ExampleFieldsScreen::class)->name('platform.example.fields');
Route::screen('example-layouts', ExampleLayoutsScreen::class)->name('platform.example.layouts');
Route::screen('example-charts', ExampleChartsScreen::class)->name('platform.example.charts');
Route::screen('example-editors', ExampleTextEditorsScreen::class)->name('platform.example.editors');
Route::screen('example-cards', ExampleCardsScreen::class)->name('platform.example.cards');
Route::screen('example-advanced', ExampleFieldsAdvancedScreen::class)->name('platform.example.advanced');
