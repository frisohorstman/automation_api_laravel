<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
*/

// login/authenticate via sanctum 
Route::post('login', 'App\Http\Controllers\AuthController@login');
Route::post('logout', 'App\Http\Controllers\AuthController@logout')->middleware('auth:sanctum');


// return authenticated user (is unused...)
// Route::middleware('auth:sanctum')->get('user', function (Request $request) {
//     return $request->user();
// })->name('api.auth');


// protect all our api routes via sanctum
// Route::group(['middleware' => ['auth.friso']], function () {
// Route::group(['middleware' => ['no.auth.on.network']], function () {
    // overview (api.server.home/api)
    Route::get('', 'App\Http\Controllers\OverviewController@index')->name('api.index');

    // movies
    Route::get('movies', 'App\Http\Controllers\MoviesController@index')->name('api.movies');
    Route::get('omdb/search', 'App\Http\Controllers\OmdbApiController@search')->name('api.omdb.search');

    // files
    Route::get('files', 'App\Http\Controllers\FilesController@index')->name('api.files');
    Route::get('files/watched', 'App\Http\Controllers\FilesController@watched')->name('api.files.watched');
    Route::get('files/{file}', 'App\Http\Controllers\FilesController@show')->name('api.files.show');
    Route::get('files/{file}/download', 'App\Http\Controllers\FilesController@download')->name('api.files.download');

    // voice
    Route::get('voice/functions', 'App\Http\Controllers\VoiceFunctionsController@index')->name('api.voice.functions');
    Route::get('voice/notes', 'App\Http\Controllers\VoiceNotesController@index')->name('api.voice.notes');

    // zway / zwave
    Route::get('zwave', 'App\Http\Controllers\ZwaveController@index')->name('api.zwave');

    // devices
    Route::get('devices', 'App\Http\Controllers\DevicesController@index')->name('api.devices');
    Route::get('devices/get', 'App\Http\Controllers\DevicesController@get')->name('api.devices.get');
    Route::get('devices/{device}', 'App\Http\Controllers\DevicesController@show')->name('api.devices.show');

    // infrared
    Route::get('infrared', 'App\Http\Controllers\InfraredController@index')->name('api.infrared');
    Route::get('infrared/send', 'App\Http\Controllers\InfraredController@send')->name('api.infrared.send');
// });